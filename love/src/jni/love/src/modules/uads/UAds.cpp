/**
 * Created by Martin Braun (modiX) for love2d (copied and modified from AdMob port by bio1712)
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

// LOVE
#include "common/config.h"
#include "UAds.h"
//#include "UAdsDelegate.h"
#include "config_UAds.h"

#if defined(LOVE_MACOSX)
#include <CoreServices/CoreServices.h>
#elif defined(LOVE_IOS)
#include "common/ios.h"
#elif defined(LOVE_LINUX) || defined(LOVE_ANDROID)
#include <signal.h>
#include <sys/wait.h>
#include <errno.h>
#elif defined(LOVE_WINDOWS)
#include "common/utf8.h"
#include <shlobj.h>
#include <shellapi.h>
#pragma comment(lib, "shell32.lib")
#endif
#if defined(LOVE_ANDROID)
#include "common/android.h"
#elif defined(LOVE_LINUX)
#include <spawn.h>
#endif


namespace love
{
	namespace uads
	{
		UAds::UAds()
		{
			love::android::uAdsInit(UADS_GAME_ID, UADS_TEST_MODE);
		}
		
		void UAds::test() const
		{
			printf("UADS_TEST\n");
		}

		bool UAds::isReady(const char *placementId)
		{
            return love::android::uAdsIsReady(placementId);
		}

		void UAds::show(const char *placementId)
		{
            love::android::uAdsShow(placementId);
		}
		
		std::string UAds::getLatestLoadPlacementId()
		{
            return love::android::uAdsGetLatestLoadPlacementId();
		}

        std::string UAds::getLatestStartPlacementId()
        {
            return love::android::uAdsGetLatestStartPlacementId();
        }

        std::string UAds::getLatestFinishPlacementId()
        {
            return love::android::uAdsGetLatestFinishPlacementId();
        }
		
		bool UAds::getIsUnityAdsReady(const char *placementId)
		{
            return love::android::uAdsGetIsUnityAdsReady(placementId);
		}
		
		bool UAds::getIsUnityAdsDidError()
		{
            return love::android::uAdsGetIsUnityAdsDidError();
		}
		
		std::string UAds::getUnityAdsError()
		{
            return love::android::uAdsGetUnityAdsError();
		}
		
		std::string UAds::getUnityAdsErrorMessage()
		{
            return love::android::uAdsGetUnityAdsErrorMessage();
		}
		
		bool UAds::getIsUnityAdsDidStart(const char *placementId)
		{
            return love::android::uAdsGetIsUnityAdsDidStart(placementId);
		}
		
		bool UAds::getIsUnityAdsDidFinish(const char *placementId)
		{
            return love::android::uAdsGetIsUnityAdsDidFinish(placementId);
		}
		
		std::string UAds::getUnityAdsFinishState()
		{
            return love::android::uAdsGetUnityAdsFinishState();
		}
	}
}
