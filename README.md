# RichLÖVE Mobile (Android) 0.10.2 - AdMob+UnityAds+GameServices
[//]: # (Soon: AdMob+UnityAds+InAppBilling+GameServices)

LÖVE is an *awesome* framework you can use to make 2D games in Lua. It's free, open-source, and works on Windows, Mac OS X, Linux, Android, and iOS.

RichLÖVE is an *awesome* fork that allows your game to have features that are usual for mobile platforms, such as displaying ads.

LÖVE Documentation
------------------

Use our [wiki][wiki] for documentation.
If you need further help, feel free to ask on our [forums][forums], and last but not least there's the irc channel [#love on OFTC][irc] and Discord server [LÖVE][discord].

Rich Modules
------------

- `[love.ads]----` **AdMob** (created by bio1712) *- Displays banner, interstitial and reward video ads using the native Ad-API of Google, admob.google.com)*
- `[love.uads]---` **UnityAds** *- Displays video and reward video ads using the native Ad-API of Unity3D, operate.dashboard.unity3d.com)*
- `[love.mobsvc]-` **MobileServices** *- Provides a common API for Saved Games, Leaderboards and Achievements using Play Games Services, developers.google.com/games/services*

Configuration
-------------

### AdMob ###

Configure [`love/src/main/java/org/love2d/android/AdActivity.java`](love/src/main/java/org/love2d/android/AdActivity.java) to match your Android AdMob APP-ID and, additionally for testing, your Android AdMob TEST-DEVICE-ID. If you don't have any TEST-DEVICE-ID, only use [TestAds][admobtestids] within your game. 

When debugging your game, you can optain the TEST-DEVICE-ID in the logs. After adding your TEST-DEVICE-ID, you can use real ads in development on this device.

Be careful to not display real ads without a correct TEST-DEVICE-ID, as even simple ad impressions can cause a device ban.

### UnityAds ###

Configure [`love/src/jni/love/src/modules/uads/config_UAds.h`](love/src/jni/love/src/modules/uads/config_UAds.h) to match your Android UnityAds GAME-ID and, additionally for testing, the TEST-MODE flag. Make sure that your device is added as test device or the global client testing flag is enabled on the UnityAds dashboard before disabling the TEST-MODE flag in development.

### MobileServices ###

Follow these three articles to setup the Play Games Services for your game:

- [Setting Up Google Play Games Services](https://developers.google.com/games/services/console/enabling) to learn how to setup it for your game
- [Get Started with Play Games Services for Android](https://developers.google.com/games/services/android/quickstart) for a step by step tutorial on a sample app (in case you struggle on basics)
- [Troubleshooting Issues in Your Android Game](https://developers.google.com/games/services/android/troubleshooting) to validate everything or find potential reasons in case it does not work

Configure [`love/src/jni/love/src/modules/mobsvc/config_MobSvc.h`](love/src/jni/love/src/modules/mobsvc/config_MobSvc.h). Undefine the flag `MOBSVC_ANDROID_PLAY_GAMES_PERMISSION_GDRIVE` if you don't need the Saved Games feature.

After updating your leaderboards or achievements find the *GetRessources* link in the leaderboards or achievements section of the Google Play Console. Save the returning XML data in [`/src/app/src/main/res/values/games-ids.xml`](/src/app/src/main/res/values/games-ids.xml)

Compilation
-----------

Open the root folder of this project in Android Studio. Let Gradle sync all your project files and build.

API
---

For the API documentation, please visit the [forum thread of RichLÖVE][forumthread].

[site]: http://love2d.org
[wiki]: http://love2d.org/wiki
[forums]: http://love2d.org/forums
[irc]: irc://irc.oftc.net/love
[discord]: https://discord.gg/H3zQShj
[admobtestids]: https://developers.google.com/admob/android/test-ads
[forumthread]: https://love2d.org/forums/viewtopic.php?f=5&t=85468&p=221993#p221993