/**
 * Created by Martin Braun (Marty) for love2d
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

package org.love2d.android;

import android.os.Bundle;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import android.app.AlertDialog;
import android.content.DialogInterface;


// UAds
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;

// MobSvc
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.PlayersClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.AchievementsClient;
import com.google.android.gms.games.AnnotatedData;
import com.google.android.gms.games.LeaderboardsClient;
import com.google.android.gms.games.SnapshotsClient;
import com.google.android.gms.games.snapshot.SnapshotMetadataBuffer;
import com.google.android.gms.games.snapshot.SnapshotMetadata;
import com.google.android.gms.games.snapshot.Snapshot;
import com.google.android.gms.games.snapshot.SnapshotMetadataChange;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


// Pay


// Firebase


public class RichGameActivity extends AdActivity {

	private static final String TAG = "RichGameActivity";

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		/*if (!isTaskRoot()
				&& getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
				&& getIntent().getAction() != null
				&& getIntent().getAction().equals(Intent.ACTION_MAIN)) {
			finish();
			return;
		}*/
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStart() {
		super.onStart();

	}
	
	@Override
    protected void onDestroy() {
		super.onDestroy();
		System.exit(0); // rough, make sure all threads getting killed (hack)
    }

    @Override
    protected void onPause() {
		super.onPause();

    }

    @Override
    public void onResume() {
		super.onResume();

    }

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch(requestCode) {
			case MOBSVC_SIGN_IN_REQ:
				mobSvcSignInActivityCallback(resultCode, data);
				break;
		}
	}

	@Override
	protected void onNewIntent (Intent intent) {
		/* This function causes trouble, so we shallow it
		The function in the original activity was finishing the existent intent and creating a new one, causing the game to restart!
		 */
	};

    //region UAds

	abstract class UnityAdsListener implements IUnityAdsListener {
		public String loadPlacementId;
		public String startPlacementId;
		public String finishPlacementId;
		public boolean unityAdsReady;
		public boolean unityAdsDidError;
		public UnityAds.UnityAdsError unityAdsError;
		public String unityAdsErrorMessage;
		public boolean unityAdsDidStart;
		public boolean unityAdsDidFinish;
		public UnityAds.FinishState unityAdsDidFinishState;
		abstract void initProperties();
	}
	private UnityAdsListener unityAdsListener;

	public void uAdsInit(final String gameId, boolean testMode) {
		this.uAdsInitUnityAdsListener();
		this.unityAdsListener.initProperties();
		UnityAds.initialize(this, gameId, this.unityAdsListener, testMode);
		Log.d(TAG,"Unity ads initialised.");
	}

	public boolean uAdsIsReady(final String placementId) {
		boolean ret = UnityAds.isReady(placementId);
		Log.d(TAG,"Unity ad "+placementId+" ready: "+ret);
		return ret;
	}

	public void uAdsShow(final String placementId) {
		UnityAds.show(this, placementId);
		Log.d(TAG,"Unity ad shown.");
	}

	public String uAdsGetLatestLoadPlacementId() {
		String ret = this.unityAdsListener.loadPlacementId;
		//Log.d(TAG,"Unity ad latest load placement ID: "+ret);
		return ret;
	}

	public String uAdsGetLatestStartPlacementId() {
		String ret = this.unityAdsListener.startPlacementId;
		//Log.d(TAG,"Unity ad latest start placement ID: "+ret);
		return ret;
	}

	public String uAdsGetLatestFinishPlacementId() {
		String ret = this.unityAdsListener.finishPlacementId;
		//Log.d(TAG,"Unity ad latest finish placement ID: "+ret);
		return ret;
	}

	public boolean uAdsGetIsUnityAdsReady(final String placementId) {
		if (this.unityAdsListener.unityAdsReady && this.unityAdsListener.loadPlacementId.equals(placementId))
		{
			Log.d(TAG,"Unity ad "+placementId+" is ready. (polling)");
			this.unityAdsListener.unityAdsReady = false; // reset property
			return true;
		}
		//Log.d(TAG,"Unity ad "+placementId+" is not ready. (polling)");
		return false;
	}

	public boolean uAdsGetIsUnityAdsDidError() {
		if (this.unityAdsListener.unityAdsDidError)
		{
			Log.d(TAG,"Unity ad did error. (polling)");
			this.unityAdsListener.unityAdsDidError = false; // reset property
			return true;
		}
		//Log.d(TAG,"Unity ad did not error. (polling)");
		return false;
	}

	public String uAdsGetUnityAdsError() {
		String ret = "";
		switch(this.unityAdsListener.unityAdsError)
		{
			case NOT_INITIALIZED: ret = "NotInitialized"; break;
			case INITIALIZE_FAILED: ret = "InitializedFailed"; break;
			case INVALID_ARGUMENT: ret = "InvalidArgument"; break;
			case VIDEO_PLAYER_ERROR: ret = "VideoPlayerError"; break;
			case INIT_SANITY_CHECK_FAIL: ret = "InitSanityCheckFail"; break;
			case AD_BLOCKER_DETECTED: ret = "AdBlockerDetected"; break;
			case FILE_IO_ERROR: ret = "FileIoError"; break;
			case DEVICE_ID_ERROR: ret = "DeviceIdError"; break;
			case SHOW_ERROR: ret = "ShowError"; break;
			case INTERNAL_ERROR: ret = "InternalError"; break;
			default: ret = "NotInitialized";
		}
		this.unityAdsListener.unityAdsError = UnityAds.UnityAdsError.NOT_INITIALIZED; // reset property
		//Log.d(TAG,"Unity ad error: "+ret+" (polling)");
		return ret;
	}

	public String uAdsGetUnityAdsErrorMessage() {
		String ret = this.unityAdsListener.unityAdsErrorMessage;
		//Log.d(TAG,"Unity ad error message: "+ret+" (polling)");
		return ret;
	}

	public boolean uAdsGetIsUnityAdsDidStart(final String placementId) {
		if (this.unityAdsListener.unityAdsDidStart && this.unityAdsListener.startPlacementId.equals(placementId))
		{
			Log.d(TAG,"Unity ad "+placementId+" did start. (polling)");
			this.unityAdsListener.unityAdsDidStart = false; // reset property
			return true;
		}
		//Log.d(TAG,"Unity ad "+placementId+" did not start. (polling)");
		return false;
	}

	public boolean uAdsGetIsUnityAdsDidFinish(final String placementId) {
		if (this.unityAdsListener.unityAdsDidFinish && this.unityAdsListener.finishPlacementId.equals(placementId))
		{
			Log.d(TAG,"Unity ad "+placementId+" did finish. (polling)");
			this.unityAdsListener.unityAdsDidFinish = false; // reset property
			return true;
		}
		//Log.d(TAG,"Unity ad "+placementId+" did not finish. (polling)");
		return false;
	}

	public String uAdsGetUnityAdsFinishState() {
		String ret = "";
		switch(this.unityAdsListener.unityAdsDidFinishState)
		{
			case ERROR: ret = "Error"; break;
			case SKIPPED: ret = "Skipped"; break;
			case COMPLETED: ret = "Completed"; break;
			default: ret = "Error";
		}
		this.unityAdsListener.unityAdsDidFinishState = UnityAds.FinishState.ERROR; // reset property
		Log.d(TAG,"Unity ad finish state: "+ret+" (polling)");
		return ret;
	}

	private void uAdsInitUnityAdsListener() {
		this.unityAdsListener = new UnityAdsListener() {

			public void initProperties() {
				this.loadPlacementId = "";
				this.startPlacementId = "";
				this.finishPlacementId = "";
				this.unityAdsReady = false;
				this.unityAdsDidError = false;
				this.unityAdsError = UnityAds.UnityAdsError.NOT_INITIALIZED;
				this.unityAdsErrorMessage = "";
				this.unityAdsDidStart = false;
				this.unityAdsDidFinish = false;
				this.unityAdsDidFinishState = UnityAds.FinishState.ERROR;
			}

			@Override
			public void onUnityAdsReady(String s) {
				Log.d("RichActivity","Unity ad "+s+" ready.");
				this.loadPlacementId = s;
				this.unityAdsReady = true;
			}

			@Override
			public void onUnityAdsStart(String s) {
				Log.d("RichActivity","Unity ad "+s+" did start.");
				this.startPlacementId = s;
				this.unityAdsDidStart = true;
			}

			@Override
			public void onUnityAdsFinish(String s, UnityAds.FinishState finishState) {
				Log.d("RichActivity","Unity ad "+s+" did finish.");
				this.finishPlacementId = s;
				this.unityAdsDidFinishState = finishState;
				this.unityAdsDidFinish = true;
			}

			@Override
			public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String s) {
				Log.d("RichActivity","Unity ad error: "+s);
				this.unityAdsError = unityAdsError;
				this.unityAdsErrorMessage = s;
				this.unityAdsDidError = true;
			}

		};
	}

	//endregion

	//region MobSvc

	private static final int MOBSVC_SIGN_IN_REQ = 0x10;

	private boolean mobSvcAvailable = false;
	private GoogleSignInClient mobSvcClient;
	private GoogleSignInAccount mobSvcAccount;
	private PlayersClient mobSvcPlayerClient;
	private String mobSvcPlayerId;
	private SnapshotsClient mobSvcSnapshotsClient;
	private LeaderboardsClient mobSvcLeaderboardsClient;
	private AchievementsClient mobSvcAchievementsClient;

	private void alertMissingGms() {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Google Play Games Unavailable");
		alertDialog.setMessage("Player is not signed in");
		alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		alertDialog.show();
	}

	public void mobSvcInit(final boolean allowGDrive) {
		// Determine if Google Play Games Services is installed on the device to make this optional. Will treat disabled services as installed.
		PackageManager pm = context.getPackageManager();
		//boolean isKindle = (Build.MANUFACTURER.equals("Amazon") && Build.MODEL.equals("Kindle Fire")) || Build.MODEL.startsWith("KF");
		try
		{
			PackageInfo info = pm.getPackageInfo("com.google.android.play.games", PackageManager.GET_ACTIVITIES);
			String label = (String) info.applicationInfo.loadLabel(pm);
			mobSvcAvailable = (label != null && !label.equals("Google Play"));

			if(mobSvcAvailable) {
				GoogleSignInOptions.Builder gsoBuilder = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
				if(allowGDrive) {
					gsoBuilder = gsoBuilder.requestScopes(Drive.SCOPE_APPFOLDER);
				}
				GoogleSignInOptions gso = gsoBuilder.build();
				mobSvcClient = GoogleSignIn.getClient(this, gso);
				Log.d(TAG,"Play Games Services initialised.");
			}
		}
		catch (PackageManager.NameNotFoundException e) { }
	}

	boolean mobSvcSignInWait = false;
	public String mobSvcSignInAwait() {
		if(mobSvcAvailable && !mobSvcSignInWait) {
			mobSvcPlayerId = null;
			mobSvcAccount = GoogleSignIn.getLastSignedInAccount(this);
			if (mobSvcAccount == null && mobSvcClient != null) {
				mobSvcSignInWait = true;
				mobSvcClient.silentSignIn().addOnCompleteListener(this, new OnCompleteListener<GoogleSignInAccount>() {
					@Override
					public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
						if (task.isSuccessful()) {
							// The signed in account is stored in the task's result.
							mobSvcAccount = task.getResult();
							mobSvcSignInWait = false;
						} else {
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									// Player will need to sign-in explicitly using via UI
									Intent intent = mobSvcClient.getSignInIntent();
									startActivityForResult(intent, MOBSVC_SIGN_IN_REQ);
								}
							});
						}
					}
				});
			} // meanwhile:
			while (mobSvcSignInWait) SystemClock.sleep(100); // await, runs forever if the player won't login (just to stay consistent, because iOS provides no deny/fail callback)
			if(mobSvcAccount != null) {
				mobSvcSignInWait = true;
				mobSvcPlayerClient = Games.getPlayersClient(this, mobSvcAccount);
				mobSvcPlayerClient.getCurrentPlayerId().addOnCompleteListener(this, new OnCompleteListener<String>() {
					@Override
					public void onComplete(@NonNull Task<String> task) {
						if (task.isSuccessful()) {
							mobSvcPlayerId = task.getResult();
							Log.d(TAG, "Successfully signed into Play Games Services.");
						} else {
							mobSvcPlayerId = null;
							Log.d(TAG, "Successfully signed into Play Games Services, but failed receiving the player ID.");
						}
						mobSvcSignInWait = false;
					}
				}); // meanwhile:
				mobSvcSnapshotsClient = Games.getSnapshotsClient(this, mobSvcAccount);
				mobSvcLeaderboardsClient = Games.getLeaderboardsClient(this, mobSvcAccount);
				mobSvcAchievementsClient = Games.getAchievementsClient(this, mobSvcAccount);
				while (mobSvcSignInWait) SystemClock.sleep(100); // await getting player ID
			}
			return mobSvcPlayerId;
		}
		return null;
	}
	private void mobSvcSignInActivityCallback(final int resultCode, final Intent data) {
		GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
		if (result.isSuccess()) {
			// The signed in account is stored in the result.
			mobSvcAccount = result.getSignInAccount();
		} else {
			Status status = result.getStatus();
			String message = status.getStatusMessage();
			if (message == null || message.isEmpty()) {
				message = "Unknown Error on Google Sign-In.";
			}
			Log.d(TAG, message + " " + status.toString());
		}
		mobSvcSignInWait = false;
	}

	public boolean mobSvcIsSignedIn() {
		return GoogleSignIn.getLastSignedInAccount(this) != null;
	}

	boolean mobSvcDownloadSavedGamesMetadataWait = false;
	public String[] mobSvcDownloadSavedGamesMetadataAwait() {
		mobSvcDownloadSavedGamesMetadataWait = false;
		final List<String> retList = new ArrayList<String>();
		if(mobSvcSnapshotsClient != null) {
			mobSvcDownloadSavedGamesMetadataWait = true;
			mobSvcSnapshotsClient.load(false).addOnCompleteListener(new OnCompleteListener<AnnotatedData<SnapshotMetadataBuffer>>() {
				@Override
				public void onComplete(@NonNull Task<AnnotatedData<SnapshotMetadataBuffer>> task) {
					if(task.isSuccessful()) {
						AnnotatedData<SnapshotMetadataBuffer> result = task.getResult();
						List<String> check = new ArrayList<String>();
						if (!result.isStale()) {
							SnapshotMetadataBuffer snapShotMetadataBuffer = result.get();
							if(snapShotMetadataBuffer != null) {
								for (SnapshotMetadata s : snapShotMetadataBuffer) {
									String name = s.getUniqueName();
									if(!check.contains(name)) {
										check.add(name);
										retList.add(name);
										retList.add(s.getDescription());
										retList.add(Long.toString(s.getLastModifiedTimestamp() / 1000L));
									}
								} // Each save game has 3 metadata entries
								snapShotMetadataBuffer.release();
							}
							mobSvcDownloadSavedGamesMetadataWait = false;

						} else {
							Log.w(TAG, "Failed to load saved games: The result was successfully, but the data was staled.");
						}
					} else {
						Exception ex = task.getException();
						Log.d(TAG, "Failed to downlaod saved games metadata: " + (ex != null ? ex.getMessage() : "UNKNOWN"));
					}
				}
			});
		} // meanwhile:
		while (mobSvcDownloadSavedGamesMetadataWait) SystemClock.sleep(100); // await getting savegame metadata
		return retList.toArray(new String[0]); // letting toArray allocate a new array instead of using .size() is faster
	}

	boolean mobSvcDownloadSavedGameMetadataWait = false;
	public String[] mobSvcDownloadSavedGameMetadataAwait(final String name) {
		mobSvcDownloadSavedGameMetadataWait = false;
		final List<String> retList = new ArrayList<String>();
		if(mobSvcSnapshotsClient != null) {
			mobSvcDownloadSavedGameMetadataWait = true;
			mobSvcSnapshotsClient.load(false).addOnCompleteListener(new OnCompleteListener<AnnotatedData<SnapshotMetadataBuffer>>() {
				@Override
				public void onComplete(@NonNull Task<AnnotatedData<SnapshotMetadataBuffer>> task) {
					if(task.isSuccessful()) {
						AnnotatedData<SnapshotMetadataBuffer> result = task.getResult();
						if (!result.isStale()) {
							SnapshotMetadataBuffer snapshotMetadataBuffer = result.get();
							if(snapshotMetadataBuffer != null) {
								for (SnapshotMetadata s : snapshotMetadataBuffer) {
									if (s.getUniqueName().equals(name)) {
										retList.add(s.getUniqueName());
										retList.add(s.getDescription());
										retList.add(Long.toString(s.getLastModifiedTimestamp() / 1000L));
										break;
									}
								} // Each save game has 3 metadata entries
								snapshotMetadataBuffer.release();
							}
							mobSvcDownloadSavedGameMetadataWait = false;

						} else {
							Log.w(TAG, "Failed to load saved games: The result was successfully, but the data was staled.");
						}
					} else {
						Exception ex = task.getException();
						Log.d(TAG, "Failed to download saved game metadata: " + (ex != null ? ex.getMessage() : "UNKNOWN"));
					}
				}
			});
		} // meanwhile:
		while (mobSvcDownloadSavedGameMetadataWait) SystemClock.sleep(100); // await getting savegame metadata
		return retList.toArray(new String[0]); // letting toArray allocate a new array instead of using .size() is faster
	}

	boolean mobSvcDownloadSavedGameDataWait = false;
	String mobSvcDownloadSavedGameCache;
	public String mobSvcDownloadSavedGameDataAwait(final String name) {
		mobSvcDownloadSavedGameDataWait = false;
		if(mobSvcSnapshotsClient != null) {
			mobSvcDownloadSavedGameDataWait = true;
			mobSvcSnapshotsClient.open(name, true, SnapshotsClient.RESOLUTION_POLICY_MOST_RECENTLY_MODIFIED).addOnFailureListener(new OnFailureListener() {
				@Override
				public void onFailure(@NonNull Exception e) {
					Log.e(TAG, "Error while opening snapshot: ", e);
					mobSvcUploadSavedGameDataWait = false;
				}
			}).continueWith(new Continuation<SnapshotsClient.DataOrConflict<Snapshot>, byte[]>() {
				@Override
				public byte[] then(@NonNull Task<SnapshotsClient.DataOrConflict<Snapshot>> task) throws Exception {
					Snapshot snapshot = task.getResult().getData();
					// Opening the snapshot was a success and any conflicts have been resolved.
					try {
						// Extract the raw data from the snapshot.
						return snapshot.getSnapshotContents().readFully();
					} catch (IOException e) {
						Log.e(TAG, "Error while reading snapshot: ", e);
					} catch (NullPointerException e) {
						Log.e(TAG, "Error while reading snapshot: ", e);
					}
					return null;
				}
			}).addOnCompleteListener(new OnCompleteListener<byte[]>() {
				@Override
				public void onComplete(@NonNull Task<byte[]> task) {
					if(task.isSuccessful()) {
						byte[] data = task.getResult();
						try {
							mobSvcDownloadSavedGameCache = new String(data, "UTF-16BE");
						} catch (UnsupportedEncodingException e) {
							Log.d(TAG, "Failed to deserialize save game data: " + e.getMessage());
						}
					} else {
						Exception ex = task.getException();
						Log.d(TAG, "Failed to load saved game data: " + (ex != null ? ex.getMessage() : "UNKNOWN"));
					}
					mobSvcDownloadSavedGameDataWait = false;
				}
			});
		} // meanwhile:
		while (mobSvcDownloadSavedGameDataWait) SystemClock.sleep(100); // await getting savegame data.
		String ret = mobSvcDownloadSavedGameCache;
		mobSvcDownloadSavedGameCache = null;
		return ret;
	}

	boolean mobSvcUploadSavedGameDataWait = false;
	boolean mobSvcUploadSavedGameDataResult = false;
	public boolean mobSvcUploadSavedGameDataAwait(final String name, final String data, final String description)
	{
		mobSvcUploadSavedGameDataWait = false;
		mobSvcUploadSavedGameDataResult = false;
		if(mobSvcSnapshotsClient != null) {
			mobSvcUploadSavedGameDataWait = true;
			mobSvcSnapshotsClient.open(name, true, SnapshotsClient.RESOLUTION_POLICY_MOST_RECENTLY_MODIFIED).addOnFailureListener(new OnFailureListener() {
				@Override
				public void onFailure(@NonNull Exception e) {
					Log.e(TAG, "Error while opening snapshot: ", e);
					mobSvcUploadSavedGameDataWait = false;
				}
			}).continueWith(new Continuation<SnapshotsClient.DataOrConflict<Snapshot>, Task<SnapshotMetadata>>() {
				@Override
				public Task<SnapshotMetadata> then(@NonNull Task<SnapshotsClient.DataOrConflict<Snapshot>> task) throws Exception {
					Snapshot snapshot = task.getResult().getData();
					// Opening the snapshot was a success and any conflicts have been resolved.
					try {
						snapshot.getSnapshotContents().writeBytes(data.getBytes("UTF-16BE"));
						SnapshotMetadataChange metadataChange = new SnapshotMetadataChange.Builder().setDescription(description).build();
						return mobSvcSnapshotsClient.commitAndClose(snapshot, metadataChange).addOnCompleteListener(new OnCompleteListener<SnapshotMetadata>() {
							@Override
							public void onComplete(@NonNull Task<SnapshotMetadata> task) {
								if(task.isSuccessful()) {
									mobSvcUploadSavedGameDataResult = true;
								} else {
									Exception ex = task.getException();
									Log.d(TAG, "Failed to save saved game data: " + (ex != null ? ex.getMessage() : "UNKNOWN"));
								}
								mobSvcUploadSavedGameDataWait = false;
							}
						});
					} catch (IOException e) {
						Log.e(TAG, "Error while reading snapshot: ", e);
						mobSvcUploadSavedGameDataWait = false;
					}
					return null;
				}
			});
		} // meanwhile:
		while (mobSvcUploadSavedGameDataWait) SystemClock.sleep(100); // await uploading savegame data.
		return mobSvcUploadSavedGameDataResult;
	}

	SnapshotMetadataBuffer mobSvcDeleteSavedGameSnapshotBuffer = null;
	boolean mobSvcDeleteSavedGameWait = false;
	boolean mobSvcDeleteSavedGameResult = false;
	public boolean mobSvcDeleteSavedGameAwait(final String name)
	{
		mobSvcDeleteSavedGameWait = false;
		mobSvcDeleteSavedGameResult = false;
		if(mobSvcSnapshotsClient != null) {
			mobSvcDeleteSavedGameWait = true;
			mobSvcSnapshotsClient.load(false).addOnCompleteListener(new OnCompleteListener<AnnotatedData<SnapshotMetadataBuffer>>() {
				@Override
				public void onComplete(@NonNull Task<AnnotatedData<SnapshotMetadataBuffer>> task) {
					if(task.isSuccessful()) {
						AnnotatedData<SnapshotMetadataBuffer> result = task.getResult();
						mobSvcDeleteSavedGameSnapshotBuffer = result.get();
						if(mobSvcDeleteSavedGameSnapshotBuffer != null) {
							boolean foundOne = false;
							for (SnapshotMetadata s : mobSvcDeleteSavedGameSnapshotBuffer) {
								if (s.getUniqueName().equals(name)) {
									foundOne = true;
									mobSvcSnapshotsClient.delete(s).addOnCompleteListener(new OnCompleteListener<String>() {
										@Override
										public void onComplete(@NonNull Task<String> task) {
											if (task.isSuccessful()) {
												mobSvcDeleteSavedGameResult = true;
											} else {
												Log.d(TAG, "Failed to delete save game data: " + task.getException().getMessage());
											}
											mobSvcDeleteSavedGameWait = false;
										}
									});
									break;
								}
							} // Each save game has 3 metadata entries
							if(!foundOne) {
								mobSvcDeleteSavedGameWait = false;
							}
						}
						else
						{
							mobSvcDeleteSavedGameWait = false;
						}
					} else {
						Exception ex = task.getException();
						Log.d(TAG, "Failed to load saved games: " + (ex != null ? ex.getMessage() : "UNKNOWN"));
					}
				}
			});
		} // meanwhile:
		while (mobSvcDeleteSavedGameWait) SystemClock.sleep(100); // await getting savegame deleted.
		if(mobSvcDeleteSavedGameSnapshotBuffer != null) {
			mobSvcDeleteSavedGameSnapshotBuffer.release();
			mobSvcDeleteSavedGameSnapshotBuffer = null;
		}
		return mobSvcDeleteSavedGameResult;
	}

	public void mobSvcShowLeaderboards() {
		if(mobSvcLeaderboardsClient != null) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mobSvcLeaderboardsClient.getAllLeaderboardsIntent().addOnCompleteListener(new OnCompleteListener<Intent>() {
						@Override
						public void onComplete(@NonNull Task<Intent> task) {
							if(task.isSuccessful()) {
								Intent intent = task.getResult();
								startActivityForResult(intent, 0x00);
							}
							else {
								Exception ex = task.getException();
								Log.d(TAG, "Failed to load leaderboard: " + (ex != null ? ex.getMessage() : "UNKNOWN"));
							}
						}
					});
				}
			});
		}
		else {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					alertMissingGms();
				}
			});
		}
	}

	public void mobSvcShowLeaderboard(final String leaderboardId) {
		if(mobSvcLeaderboardsClient != null) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mobSvcLeaderboardsClient.getLeaderboardIntent(leaderboardId).addOnCompleteListener(new OnCompleteListener<Intent>() {
						@Override
						public void onComplete(@NonNull Task<Intent> task) {
							if(task.isSuccessful()) {
								Intent intent = task.getResult();
								startActivityForResult(intent, 0x00);
							}
							else {
								Exception ex = task.getException();
								Log.d(TAG, "Failed to load leaderboard: " + (ex != null ? ex.getMessage() : "UNKNOWN"));
							}
						}
					});
				}
			});
		}
		else {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					alertMissingGms();
				}
			});
		}
	}

	boolean mobSvcUploadLeaderboardScoreWait = false;
	boolean mobSvcUploadLeaderboardScoreResult = false;
	public boolean mobSvcUploadLeaderboardScoreAwait(final String leaderboardId, long score, final String format) {
		if(mobSvcLeaderboardsClient != null) {
			mobSvcUploadLeaderboardScoreResult = false;
			if(mobSvcUploadLeaderboardScoreWait) {
				mobSvcUploadLeaderboardScoreWait = false;
				SystemClock.sleep(2000); // give other threads of the same operation time to abort, this will result in false operations that can still succeed in background, later. To avoid problems, please nest same operations in callbacks
			}
			mobSvcUploadLeaderboardScoreWait = true;
			switch (format) {
				/* values without format are simple score values in the smallest possible unit that which will be multiplied to match the online configuration on Google's end.
				 * I.E. the score value 1234 will be converted to 12.34 if 2 decimal placeholders are configured in the Google backend. This is the same like on Apple's API for Android devices.
				 */
				case "none":
					break;
				/* time values need to be converted to 1/1,000th of a second (milliseconds) for the Google API. */
				case "minutes":
					score *= 60000;
					break;
				case "seconds":
					score *= 1000;
					break;
				case "milliseconds":
					score *= 1;
					break;
				/* currency values need to be converted to 1/1,000,000th of the unit of the currency. */
				case "currencyunits":
					score *= 1000000;
					break;
				case "currencycents": // only 2 decimal placeholder currencies are allowed, because GameKit does not support other formats.
					score *= 10000;
					break;
			}
			mobSvcLeaderboardsClient.submitScoreImmediate(leaderboardId, score).addOnCompleteListener(new OnCompleteListener<ScoreSubmissionData>() {
				@Override
				public void onComplete(@NonNull Task<ScoreSubmissionData> task) {
					if(task.isSuccessful()) {
						mobSvcUploadLeaderboardScoreResult = true;
					} else {
						mobSvcUploadLeaderboardScoreResult = false;
						Exception ex = task.getException();
						Log.d(TAG, "Failed to upload leaderboard score: " + (ex != null ? ex.getMessage() : "UNKNOWN"));
					}
					mobSvcUploadLeaderboardScoreWait = false;
				}
			}); // meanwhile:
			while (mobSvcUploadLeaderboardScoreWait) SystemClock.sleep(100); // await result.
			return mobSvcUploadLeaderboardScoreResult;
		}
		return false;
	}

	public void mobSvcShowAchievements() {
		if(mobSvcAchievementsClient != null) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mobSvcAchievementsClient.getAchievementsIntent().addOnCompleteListener(new OnCompleteListener<Intent>() {
						@Override
						public void onComplete(@NonNull Task<Intent> task) {
							if(task.isSuccessful()) {
								Intent intent = task.getResult();
								startActivityForResult(intent, 0x00);
							}
							else {
								Exception ex = task.getException();
								Log.d(TAG, "Failed to load achievements: " + (ex != null ? ex.getMessage() : "UNKNOWN"));
							}
						}
					});
				}
			});
		}
		else {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					alertMissingGms();
				}
			});
		}
	}

	boolean mobSvcIncrementAchievementProgressWait = false;
	boolean mobSvcIncrementAchievementProgressResult = false;
	boolean mobSvcIncrementAchievementNowUnlocked = false;
	public boolean[] mobSvcIncrementAchievementProgressAwait(final String achievementsId, int steps, int maxSteps) {
		if(mobSvcAchievementsClient != null && steps > 0) {
			mobSvcIncrementAchievementProgressResult = false;
			mobSvcIncrementAchievementNowUnlocked = false;
			if(mobSvcIncrementAchievementProgressWait) {
				mobSvcIncrementAchievementProgressWait = false;
				SystemClock.sleep(2000); // give other threads of the same operation time to abort, this will result in false operations that can still succeed in background, later. To avoid problems, please nest same operations in callbacks
			}
			mobSvcIncrementAchievementProgressWait = true;
			if(maxSteps < 2) { // On maxSteps 1 we know this is not an incremental achievement, so we simply solve it when any step is given
				mobSvcAchievementsClient.unlockImmediate(achievementsId).addOnCompleteListener(new OnCompleteListener<Void>() {
					@Override
					public void onComplete(@NonNull Task<Void> task) {
						if(task.isSuccessful()) {
							mobSvcIncrementAchievementProgressResult = true;
							mobSvcIncrementAchievementNowUnlocked = true;
						} else {
							mobSvcIncrementAchievementProgressResult = false;
							mobSvcIncrementAchievementNowUnlocked = false;
							Exception ex = task.getException();
							Log.d(TAG, "Failed to unlock achievement: " + (ex != null ? ex.getMessage() : "UNKNOWN"));
						}
						mobSvcIncrementAchievementProgressWait = false;
					}
				});
			} else { // on maxStep 2 or more we know this is an incremental achievement, so we simply pass the step value through
				mobSvcAchievementsClient.incrementImmediate(achievementsId, steps).addOnCompleteListener(new OnCompleteListener<Boolean>() {
					@Override
					public void onComplete(@NonNull Task<Boolean> task) {
						if(task.isSuccessful()) {
							mobSvcIncrementAchievementProgressResult = true;
							mobSvcIncrementAchievementNowUnlocked = task.getResult(); // BUG: This is always false, even when the achievement is unlocked now, so we have to do:
							mobSvcAchievementsClient.load(false).addOnCompleteListener(new OnCompleteListener<AnnotatedData<AchievementBuffer>>() {
								@Override
								public void onComplete(@NonNull Task<AnnotatedData<AchievementBuffer>> task) {
									if(task.isSuccessful()) {
										AnnotatedData<AchievementBuffer> achBufferData = task.getResult();
										AchievementBuffer achBuffer = achBufferData.get();
										if(achBuffer != null) {
											for (Achievement ach : achBuffer) {
												if (ach.getAchievementId().equals(achievementsId)) {
													mobSvcIncrementAchievementNowUnlocked = ach.getState() == Achievement.STATE_UNLOCKED;
													break;
												}
											}
										}
										achBuffer.release();
									} else {
										Exception ex = task.getException();
										Log.d(TAG, "Failed to determine achievement status: " + (ex != null ? ex.getMessage() : "UNKNOWN"));
									}
									mobSvcIncrementAchievementProgressWait = false;
								}
							});
						} else {
							mobSvcIncrementAchievementProgressResult = false;
							mobSvcIncrementAchievementNowUnlocked = false;
							mobSvcIncrementAchievementProgressWait = false;
							Exception ex = task.getException();
							Log.d(TAG, "Failed to increment achievement progress: " + (ex != null ? ex.getMessage() : "UNKNOWN"));
						}
					}
				});
			} // meanwhile:
			while (mobSvcIncrementAchievementProgressWait) SystemClock.sleep(100); // await result.
			return new boolean[] { mobSvcIncrementAchievementProgressResult, mobSvcIncrementAchievementNowUnlocked };
		}
		return new boolean[] { false, false };
	}

	//endregion
}