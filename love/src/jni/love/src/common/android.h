/**
 * Copyright (c) 2006-2016 LOVE Development Team
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 **/

/**
* This file has been edited by bio1712 and Marty (Martin Braun) for love2d.org
**/

#ifndef LOVE_ANDROID_H
#define LOVE_ANDROID_H

#include "config.h"

#ifdef LOVE_ANDROID

#include <string>
#include <vector>

namespace love
{
namespace android
{

/**
 * Enables or disables immersive mode where the navigation bar is hidden.
 **/
void setImmersive(bool immersive_active);
bool getImmersive();

/**
 * Gets the scale factor of the window's screen, e.g. on Retina displays this
 * will return 2.0.
 **/
double getScreenScale();

/**
 * Gets the selected love file in the device filesystem.
 **/
const char *getSelectedGameFile();

bool openURL(const std::string &url);

void vibrate(double seconds);

/*
 * Helper functions for the filesystem module
 */
void freeGameArchiveMemory(void *ptr);

bool loadGameArchiveToMemory(const char *filename, char **ptr, size_t *size);

bool directoryExists(const char *path);

bool mkdir(const char *path);

bool createStorageDirectories();

//Ads
void createBanner(const char *adID,const char *position);
	
void hideBanner();
	
void showBanner();

void requestInterstitial(const char *adID);

bool isInterstitialLoaded();

void showInterstitial();

void requestRewardedAd(const char *adID);

bool isRewardedAdLoaded();

void showRewardedAd();

//For callbacks
bool coreInterstitialError();

bool coreInterstitialClosed();

bool coreRewardedAdError();

bool coreRewardedAdDidStop();

bool coreRewardedAdDidFinish();

std::string coreGetRewardType();

double coreGetRewardQuantity();

    // UAds

    void uAdsInit(const char *gameId, bool testMode);
    bool uAdsIsReady(const char *placementId);
    void uAdsShow(const char *placementId);
    std::string uAdsGetLatestLoadPlacementId();
    std::string uAdsGetLatestStartPlacementId();
    std::string uAdsGetLatestFinishPlacementId();
    bool uAdsGetIsUnityAdsReady(const char *placementId);
    bool uAdsGetIsUnityAdsDidError();
    std::string uAdsGetUnityAdsError();
    std::string uAdsGetUnityAdsErrorMessage();
    bool uAdsGetIsUnityAdsDidStart(const char *placementId);
    bool uAdsGetIsUnityAdsDidFinish(const char *placementId);
    std::string uAdsGetUnityAdsFinishState();

    // MobSvc

    void mobSvcInit(bool allowGDrive);
    std::string mobSvcSignInAwait();
    bool mobSvcIsSignedIn();
    std::vector<std::string> mobSvcDownloadSavedGamesMetadataAwait();
    std::vector<std::string> mobSvcDownloadSavedGameMetadataAwait(const char *name);
    std::string mobSvcDownloadSavedGameDataAwait(const char *name);
    bool mobSvcUploadSavedGameDataAwait(const char *name, const char *data, const char *description);
    bool mobSvcDeleteSavedGameAwait(const char *name);
    void mobSvcShowLeaderboards();
    void mobSvcShowLeaderboard(const char *leaderboardId);
    bool mobSvcUploadLeaderboardScoreAwait(const char *leaderboardId, long score, const char *format);
    void mobSvcShowAchievements();
    std::vector<bool> mobSvcIncrementAchievementProgressAwait(const char *achievementId, int steps, int maxSteps);

} // android
} // love

#endif // LOVE_ANDROID
#endif // LOVE_ANDROID_H
